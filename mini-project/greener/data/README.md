# Short description of the different files and datasets

The data sets stored in the different text files (csv format) of the directory
correspond to measurements carried out during 3 full years on the GreEn-Er
building from 2017 (2017-01-01 00:00:00) to 2019 (2019-12-31 23:00:00). The
different files are shortly described hereinafter.

- `conso.csv`
  hourly measures (with timestamps) of the electrical consumption in kilo
  watt-hour (kWh) for different items that are in order
    - `Autres electricite`, all that is not included in the five following items
    - `Chauffage`, heating during the cold season,
    - `Eclairage`,
    - `Froid`, related to air-cooling during the hot season,
    - `Prise de Courant`,
    - `Ventilation`,
    - `Global Consumption`, the sum of all the above measures

- `conso_globale.csv`
  hourly measures (with timestamps) of the electrical consumption in kilo
  watt-hour (kWh) which contains only the `Global Consumption`, the sum of all
  electricity items in the building

- `weather.csv`
  hourly measures (with timestamps) that contains the outdoor temperature + some
  solar radiation measurements, that are in order:
    - `Temperature`, in Celsius degree,
    - `GHI`, the Global Horizontal Irradiation,
    - `DNI`, Direct Normal Irradiation,
    - `DHI`, Diffuse Horizontal Irradiation.

    The sun radiation are  kilo watt-hour per square metre (kWh/m^2).
    This combines Direct Normal Irradiation (DNI) and Diffuse Horizontal
    Irradiation (DHI). Both are linked in the formula for Global Horizontal
    Irradiation (GHI): GHI = DHI + DNI · cos (θ) where θ is the solar zenith
    angle.

- `calendar.csv`
  daily information (with timestamps) on the opening of the building, in order:
    - `Presence`, building opening (boolean `True` if open, `False` if closed)
    - `Day` of the week (integer coded as `0` for monday, ... , `6` for sunday)
